import React, {useState, useEffect} from 'react';
import {View, FlatList} from 'react-native';
import {List, FAB, Avatar} from 'react-native-paper';
import axios from 'axios';
import globalStyles from '../styles/global';
import config from '../config';

const User = ({navigation}) => {
  const [clientes, setUsers] = useState([]);
  const [consultarApi, setUsersApi] = useState(true);

  useEffect(() => {
    const getUsersApi = async () => {
      try {
        const url = `${config.apiUrl}/api/users`;
        const {data} = await axios.get(url);
        console.log(data);
        setUsers(data);
        setUsersApi(false);
      } catch (error) {
        console.log(error);
      }
    };

    if (consultarApi) {
      getUsersApi();
    }
  }, [consultarApi]);

  return (
    <View style={globalStyles.content}>
      <FlatList
        data={clientes}
        keyExtractor={user => user.id.toString()}
        renderItem={({item}) => (
          <List.Item
            title={item.name}
            description={item.username}
            left={props => (
              <Avatar.Icon
                {...props}
                icon="account"
                size={40}
                color="#BC986A"
              />
            )}
            onPress={() =>
              navigation.navigate('Delete', {item, setUsersApi})
            }
          />
        )}
      />

      <FAB
        icon="plus"
        style={globalStyles.fab}
        onPress={() => navigation.navigate('Item', {setUsersApi})}
      />
    </View>
  );
};

export default User;
