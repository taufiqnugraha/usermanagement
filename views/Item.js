import React, {useState, useEffect} from 'react';
import {View, StyleSheet} from 'react-native';
import {
  TextInput,
  Button,
  Paragraph,
  Dialog,
  Portal,
  Switch,
} from 'react-native-paper';
import axios from 'axios';
import globalStyles from '../styles/global';
import config from '../config';

const Item = ({navigation, route}) => {
  const [name, setName] = useState('');
  const [status, setStatus] = useState(false);
  const toggleSwitch = () => setStatus(previousState => !previousState);
  // const [status, setStatus] = useState('');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [alert, setAlert] = useState(false);

  useEffect(() => {
    if (isUpdatingRecord) {
      const {name, status, username, password} = route.params.user;

      setName(name);
      setStatus(status);
      setUsername(username);
      setPassword(password);
    }
  }, []);

  const {setUsersApi} = route.params;
  const isUpdatingRecord = !!route.params.user;

  const store = async () => {
    if (!name || !username || !password) {
      setAlert(true);
      return;
    }
    const user = {name, status, username, password};

    if (isUpdatingRecord) {
      try {
        const url = `${config.apiUrl}/api/users/${route.params.user.id}`;
        await axios.post(url, user);
      } catch (error) {
        console.log(error);
      }
    } else {
      try {
        const url = `${config.apiUrl}/api/users`;
        await axios.post(url, user);
      } catch (error) {
        console.log(error);
      }
    }
    setUsersApi(true);
    navigation.navigate('User');
  };

  return (
    <View style={globalStyles.content}>
      <Switch
        trackColor={{false: '#767577', true: '#81b0ff'}}
        thumbColor={status ? '#f5dd4b' : '#f4f3f4'}
        ios_backgroundColor="#3e3e3e"
        onValueChange={value => setStatus(value)}
        value={status}
      />
      <TextInput
        label="Nama"
        style={styles.input}
        value={name}
        onChangeText={setName}
      />

      <TextInput
        label="Username"
        style={styles.input}
        value={username}
        onChangeText={setUsername}
      />

      <TextInput
        label="Password"
        style={styles.input}
        value={password}
        secureTextEntry={true}
        onChangeText={setPassword}
      />

      <Button mode="contained" icon="pencil-circle" uppercase onPress={store}>
        {isUpdatingRecord ? 'Save' : 'Add'}
      </Button>

      <Portal>
        <Dialog visible={alert} onDismiss={() => setAlert(false)}>
          <Dialog.Title>Error</Dialog.Title>
          <Dialog.Content>
            <Paragraph>Please fill all field</Paragraph>
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={() => setAlert(false)}>OK</Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    marginBottom: 20,
    backgroundColor: 'transparent',
  },
});

export default Item;
