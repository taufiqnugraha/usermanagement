import React from 'react';
import {View, StyleSheet, Alert} from 'react-native';
import {Headline, Subheading, Text, Button, FAB} from 'react-native-paper';
import axios from 'axios';
import globalStyles from '../styles/global';
import config from '../config';

const Delete = ({navigation, route}) => {
  const {name, status, username, password, id} = route.params.item;
  const {setUsersApi} = route.params;

  const handleDelete = () => {
    Alert.alert(
      'Delete',
      'Are you sure?',
      [
        {text: 'Cancel', style: 'cancel'},
        {
          text: 'Delete',
          onPress: async () => {
            const url = `${config.apiUrl}/api/users/${id}`;

            try {
              await axios.delete(url);
              setUsersApi(true);
              navigation.navigate('User');
            } catch (error) {
              console.log(error);
            }
          },
        },
        ,
      ],
    );
  };

  return (
    <View style={globalStyles.content}>
      <Text style={styles.text}>
        Nama: <Subheading>{name}</Subheading>
      </Text>
      <Text style={styles.text}>
        Status: <Subheading>{status == 1 ? 'Aktif' : 'Non Aktif'}</Subheading>
      </Text>
      <Text style={styles.text}>
        Email: <Subheading>{username}</Subheading>
      </Text>
      <Text style={styles.text}>
        Password: <Subheading>{password}</Subheading>
      </Text>
      <Button
        icon="pencil-circle"
        mode="contained"
        uppercase
        onPress={() => 
          navigation.navigate('Item', {
            user: route.params.item,
            setUsersApi,
          })
        }>
        Edit
      </Button>

      <FAB
        icon="delete"
        style={globalStyles.fab_delete}
        onPress={handleDelete}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    marginBottom: 20,
    fontSize: 18,
  }
});

export default Delete;
