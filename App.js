/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';

import {StyleSheet} from 'react-native';
import {DefaultTheme, Provider as PaperProvider} from 'react-native-paper';

import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator();
import User from './views/User';
import Item from './views/Item';
import Delete from './views/Delete';

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#BC986A',
    accent: '#FBEEC1',
  },
};

const App = () => {
  return (
    <PaperProvider>
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName="User"
          screenOptions={{
            headerStyle: {backgroundColor: theme.colors.primary},
            headerTintColor: theme.colors.surface,
            headerTitleStyle: {fontWeight: 'bold'},
            headerTitleAlign: 'center',
          }}>
          <Stack.Screen name="User" component={User} />
          <Stack.Screen
            name="Item"
            component={Item}
            options={({route}) => ({
              title: route.params.user ? 'Edit' : 'Add',
            })}
          />
          <Stack.Screen
            name="Delete"
            component={Delete}
            options={{
              title: 'Detail User',
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </PaperProvider>
  );
};

export default App;
