import { red } from 'color-name';
import { StyleSheet } from 'react-native'

const globalStyles = StyleSheet.create({
    content: {
        flex: 1,
        marginTop: 20,
        marginHorizontal: '2.5%'
    },
    title: {
        textAlign: 'center',
        marginTop: 20,
        marginBottom: 30,
        fontSize: 30
    },
    fab: {
        position: 'absolute',
        margin: 20,
        right: 0,
        bottom: 30
    }
    ,
    fab_delete: {
        position: 'absolute',
        margin: 20,
        right: 0,
        backgroundColor: 'red',
        bottom: 30
    }
})

export default globalStyles;